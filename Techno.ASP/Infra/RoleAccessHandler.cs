﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Techno.ASP.Tools;

namespace Techno.ASP.Infra
{
    public class RoleAccessHandler : AuthorizationHandler<RoleAccessRequirement>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, RoleAccessRequirement requirement)
        {
             if(!SessionUtils.IsLogged) context.Fail();
             if(SessionUtils.ConnectedUser != null && SessionUtils.ConnectedUser.Role== requirement.Role)
            {
                context.Succeed(requirement);
            }
            return Task.CompletedTask;
        }
    }
}
