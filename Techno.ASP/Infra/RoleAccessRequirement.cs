﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Techno.DAL.Enums;

namespace Techno.ASP.Infra
{
    public class RoleAccessRequirement :  IAuthorizationRequirement
    {
        public Role Role { get; }

        public RoleAccessRequirement(Role role)
        {
            Role = role;
        }
    }
}
