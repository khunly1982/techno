using MailKit.Net.Smtp;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Techno.ASP.Infra;
using Techno.ASP.Models;
using Techno.ASP.Models.Forms;
using Techno.ASP.Services;
using Techno.ASP.Tools;
using Techno.DAL;

namespace Techno.ASP
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            


            services.AddControllersWithViews();//Pour le mvc

            
            services.AddDistributedMemoryCache(); //Fortement recommand� 
            //Permet de s'assurer d'une impl�mentation de mise en cache m�moire 
            //sur l'h�bergement !!!! Ne fonctione qu'en single server !!!
            //Sessions
            services.AddSession(
                options => 
                    {
                        options.IdleTimeout = TimeSpan.FromMinutes(25); //Dur�e de vie
                        options.Cookie.Name = "TechoSession";
                        options.Cookie.HttpOnly = true;//Important!!Emp�che les script client de
                                                       //lire/ecrire dans le cookie
                    }
                           
                );
            //Injection IHttpContextAccessor pour permettre l'utilisation des sessions dans les classes non mvc
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            //Injection de nos services
            services.AddScoped<DataContext>();
            services.AddScoped<Mailer>();
            services.AddScoped<HttpClient>();
            services.AddScoped<SmtpClient>();

            services.AddScoped<IService<ProfileModel, ProfileForm>, ProfileService>();
            services.AddScoped<IService<SkillModel, SkillForm>,SkillService>();
            services.AddScoped<IService<SectionModel, SectionForm>,SectionService>();
            services.AddScoped<IService<UserModel, RegisterUserForm>, UserService>();

            //Ajout pour l'auhtentification
            services.AddAuthentication("CookieAuthentication")
                 .AddCookie("CookieAuthentication", config =>
                 {
                     config.Cookie.Name = "TechoSession"; // Name of cookie   
                     config.LoginPath = "/Security/Login"; // Path for the redirect to user login page  
                 });

            //Ajout des authorization
            services.AddSingleton<IAuthorizationHandler, RoleAccessHandler>();

            services.AddAuthorization(options =>
            {
                options.AddPolicy("RoleAdminPolicy", policy =>
                    policy.Requirements.Add(new RoleAccessRequirement(DAL.Enums.Role.Admin)));
                options.AddPolicy("RoleStudentPolicy", policy =>
                    policy.Requirements.Add(new RoleAccessRequirement(DAL.Enums.Role.Student)));
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            app.UseStaticFiles();

            app.UseRouting();
            app.UseSession();
            //Ajout pour l'authentication
            app.UseAuthentication();
            //Ajout pour l'autorisation
            app.UseAuthorization();
           

            //Permet � notre classe de gestion de session de r�soudre les services
            SessionUtils.Services = app.ApplicationServices;

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "areas",
                    pattern: "{area:exists}/{controller=Home}/{action=Index}/{id?}"
                  );
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
