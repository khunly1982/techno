﻿using System;
using System.Text.Json.Serialization;
using Techno.ASP.Tools;
using Techno.DAL.Enums;

namespace Techno.ASP.Models
{
    public class UserModel
    {
        public int Id { get; set; }
        public string Email { get; set; }
        [JsonIgnore]
        public byte[] PasswordOut 
        {
            get {
                return SecurityTools.HashMe(PasswordIn);
            }
         
        }
        [JsonIgnore]
        public string PasswordIn { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public DateTime? BirthDate { get; set; }
        public Role Role { get; set; }



      
    }
}
