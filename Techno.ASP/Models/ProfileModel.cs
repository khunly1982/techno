﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Techno.ASP.Models
{
    public class ProfileModel
    {
        
        public byte[] ImageFile { get; set; }
        public string ImageMimeType { get; set; }
        public string Description { get; set; }
        public string Linkedin { get; set; }
        public string Hobbies { get; set; }
        public string Studies { get; set; }
        public string ProfessionalExp { get; set; }
        public bool Published { get; set; }

        public int UserId { get; set; }
        public int? SectionId { get; set; }
        
    }
}
