﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Techno.ASP.Models
{
    public class MeteoModel
    {
        public Main main { get; set; }
    }

    public class Main
    {
        public double temp { get; set; }
        public double pressure { get; set; }
    }
}
