﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Techno.ASP.Services;

namespace Techno.ASP.Models.Forms
{
    public class ProfileForm
    {
        IService<SectionModel, SectionForm> _sectionService;
        IService<SkillModel, SkillForm> _skillService;
        public ProfileForm(IService<SectionModel, SectionForm> service, IService<SkillModel, SkillForm> skillService)
        {
            SectionService = service;
            SkillService = skillService;
        }

        public ProfileForm()
        {

        }
        public int Id { get; set; }

        [DataType(DataType.Upload)]
        public IFormFile ImageFile { get; set; }
        
        public string Description { get; set; }

        [MaxLength(255, ErrorMessage="Votre lien LinkedIn est trop long")]
        [DataType(DataType.Url)]
        public string Linkedin { get; set; }

        [MaxLength(255, ErrorMessage = "Vous avez trop d'hobbies. Pensez à travailler !!!")]
        public string Hobbies { get; set; }
        public string Studies { get; set; }
        public string ProfessionalExp { get; set; }

        public bool Published { get; set; }

        [Required]
        public int UserId { get; set; }
        public int? SectionId { get; set; }
        public List<int> SkillIds { get; set; } //récéptacle de mes choix dans le formualaire
        //Ajouter une liste de section pour la liste déroulante
        public IEnumerable<SectionModel> LesSections
        {
            get 
            {
               return SectionService?.GetAll();
            }
        }

        //Ajouter une liste pour les skills
        public IEnumerable<SkillModel> LesSkills
        {
            get { return SkillService?.GetAll(); }
        }

        public IService<SectionModel, SectionForm> SectionService
        {
            private get { return _sectionService; }
            set
            {
                _sectionService = value;
            }
        }

        public IService<SkillModel, SkillForm> SkillService
        {
            private get
            {
                return _skillService;
            }

            set
            {
                _skillService = value;
            }
        }
    }
}
