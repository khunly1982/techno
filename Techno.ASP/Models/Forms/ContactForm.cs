﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Techno.ASP.Models.Forms
{
    public class ContactForm
    {
        [Required]
        [EmailAddress]
        [MaxLength(200)]
        [Display(Name = "Votre adresse email (*)")]
        
        public string Email { get; set; }

        [Required]
        [MinLength(2)]
        [MaxLength(1000)]
        [Display(Name = "Votre message (*)")]
        public string Message { get; set; }
    }
}
