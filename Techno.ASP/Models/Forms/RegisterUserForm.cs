﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Techno.DAL.Enums;

namespace Techno.ASP.Models.Forms
{
    public class RegisterUserForm
    {
      
        public int Id { get; set; }
        [Required(ErrorMessage = "Ce champs est requis")]
        [EmailAddress]
        [MaxLength(255)]
        public string Email { get; set; }

        [Required]
        [MinLength(9,ErrorMessage ="Votre mot de passe doit contenir min 9 caractères")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [Compare("Password", ErrorMessage = "Vos mots de passe ne correspondent pas")]
        [DataType(DataType.Password)]
        public string RePassword { get; set; }

        [Required(ErrorMessage = "Ce champs est requis")]
        [MaxLength(50)]
        public string LastName { get; set; }
        [Required(ErrorMessage = "Ce champs est requis")]
        [MaxLength(50)]
        public string FirstName { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? BirthDate { get; set; }

        [Required(ErrorMessage = "Ce champs est requis")]
        [EnumDataType(typeof(Role),ErrorMessage ="Veillez choisir correctement le role")]
        public Role Role { get; set; }
    }
}
