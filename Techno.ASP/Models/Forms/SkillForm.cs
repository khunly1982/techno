﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Techno.ASP.Models.Forms
{
    public class SkillForm
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Ce champs est requis")]
        [MaxLength(50, ErrorMessage = "Le champs doit faire au max 50 caractères")]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }
    }
}
