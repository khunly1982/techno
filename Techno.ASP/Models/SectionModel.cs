﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Techno.ASP.Models
{
    public class SectionModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
