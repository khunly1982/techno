﻿$(() => {
    const $menuButton = $('#menu-button');
    const $menu = $('#menu');

    $menuButton.on('click', toggleMenu);

    function toggleMenu() {
        $menu.toggleClass('hidden');
    }
});
