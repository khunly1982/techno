﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Techno.ASP.Models;
using Techno.ASP.Models.Forms;
using Techno.ASP.Tools;
using Techno.DAL;
using Techno.DAL.Entities;

namespace Techno.ASP.Services
{
    public class UserService : IService<UserModel, RegisterUserForm>
    {
        private readonly DataContext _dc;

        public UserService(DataContext dc)
        {
            _dc = dc;
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<UserModel> GetAll()
        {
            throw new NotImplementedException();
        }

        public RegisterUserForm GetById(int id)
        {
            throw new NotImplementedException();
        }

        public UserModel GetByEmailPassword(string email, string password)
        {
          return MapToUserModel(_dc.Users.SingleOrDefault(m => m.Email == email && m.Password == SecurityTools.HashMe(password)));
        }

        private UserModel MapToUserModel(User user)
        {
            if (user == null) return null;
            return new UserModel()
            {
                Email = user.Email,
                BirthDate = user.BirthDate,
                FirstName = user.FirstName,
                LastName = user.LastName, 
                Role = user.Role
            };
        }

        public void Insert(RegisterUserForm form)
        {
            //Form => Model ==> Calculer le hash
            UserModel model = new UserModel()
            {
                Email = form.Email,
                BirthDate = form.BirthDate,
                FirstName = form.FirstName,
                LastName = form.LastName,
                PasswordIn = form.Password,
                Role = form.Role
            };

            //Model => entity ==> Sauvegarde en db
            User entity = new User()
            {
                Email = model.Email,
                BirthDate = model.BirthDate,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Password = model.PasswordOut,
                Role = model.Role
            };

            _dc.Users.Add(entity);

            _dc.SaveChanges();
            //Permet de mettre l'id calculé par la db dans l'objet de départ (Form)
            form.Id = entity.Id;
           
        }

        public void Update(RegisterUserForm form)
        {
            throw new NotImplementedException();
        }
    }
}
