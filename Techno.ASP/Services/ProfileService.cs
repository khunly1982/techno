﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Techno.ASP.Models;
using Techno.ASP.Models.Forms;
using Techno.DAL;
using Techno.DAL.Entities;

namespace Techno.ASP.Services
{
    public class ProfileService : IService<ProfileModel, ProfileForm>
    {

        private readonly DataContext _dc;

        public ProfileService(DataContext dc)
        {
            _dc = dc;
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ProfileModel> GetAll()
        {
            throw new NotImplementedException();
        }

        public ProfileForm GetById(int id)
        {
            throw new NotImplementedException();
        }

        public void Insert(ProfileForm form)
        {
            //1- Mapping du form vers le model
            ProfileModel model = new ProfileModel()
            {
                Description = form.Description,
                Hobbies = form.Hobbies,
                Linkedin = form.Linkedin,
                ProfessionalExp = form.ProfessionalExp,
                Published = form.Published,
                SectionId = form.SectionId,
                Studies = form.Studies,
                UserId = form.UserId,
                ImageMimeType = form.ImageFile?.ContentType, //Le ? ==> si c'est pas null,
                                                             //tu va chercher le ContentType
                ImageFile = UploadMe(form.ImageFile) ,
                
            };

            //2- Model vers entity
            Profile entity = new Profile()
            {
                Description = model.Description,
                Hobbies = model.Hobbies,
                Linkedin = model.Linkedin,
                ProfessionalExp = model.ProfessionalExp,
                Published = model.Published,
                SectionId = model.SectionId,
                Studies = model.Studies,
                UserId = model.UserId,
                ImageMimeType = model.ImageMimeType,
                ImageFile = model.ImageFile
            };

             _dc.Profiles.Add(entity);
             _dc.SaveChanges();

            //récupérer l'id généré par la db
            int idProfil = entity.Id;

            //remplir la table intermédiaire
            foreach (int skilId in form.SkillIds)
            {
                ProfileSkill ps = new ProfileSkill() { ProfileId = idProfil, SkillId = skilId };
                _dc.ProfileSkills.Add(ps);
            }

            _dc.SaveChanges();
             
        }

        

        public void Update(ProfileForm form)
        {
            throw new NotImplementedException();
        }

        private byte[] UploadMe(IFormFile imageFile)
        {
            if (imageFile == null) return null;

            byte[] imageEnByte = null;
            //2 -UPLOAD en bytes!!!!!!! Pas en Fichier physique!!!
            using (MemoryStream memoryStream = new MemoryStream())
            {
                imageFile.CopyTo(memoryStream);
                imageEnByte = memoryStream.ToArray();
            }

            return imageEnByte;
        }
    }
}
