﻿using MailKit.Net.Smtp;
using Microsoft.Extensions.Configuration;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Techno.ASP.Services
{
    public class Mailer
    {
        private readonly IConfiguration _config;
        private readonly SmtpClient _client;

        public Mailer(IConfiguration config, SmtpClient client)
        {
            _config = config;
            _client = client;
        }

        public bool Send(string dest, string suject, string content)
        {
            IConfiguration section = _config.GetSection("Smtp");
            // client qui permet d'envoyer des emails
            //SmtpClient client = new SmtpClient();
            // création du message qui sera envoyé
            MimeMessage message = new MimeMessage();
            // ajout au message de l'auteur
            message.From.Add(new MailboxAddress("Techno", section.GetValue<string>("Email")));
            // ajout au message du destinataire
            message.To.Add(new MailboxAddress(dest, dest));
            // ajout au message de l'objet
            message.Subject = suject;
            // ajout du contenu
            BodyBuilder builder = new BodyBuilder();
            builder.HtmlBody = content;
            message.Body = builder.ToMessageBody();

            try
            {
                _client.Connect(section.GetValue<string>("Host"), section.GetValue<int>("Port"), false);
                _client.Authenticate(section.GetValue<string>("Email"), section.GetValue<string>("Password"));

                // envoyer l'email
                _client.Send(message);
                return true;
            } catch(Exception ex)
            {
                // loguer l'erreur
                return false;
            }
        }
    }
}
