﻿using System.Collections.Generic;
using Techno.ASP.Models;
using Techno.ASP.Models.Forms;

namespace Techno.ASP.Services
{
    public interface IService<T,U>
        where T: class
        where U: class
    {
        bool Delete(int id);
        IEnumerable<T> GetAll();
        U GetById(int id);
        void Insert(U form);
        void Update(U form);
    }
}