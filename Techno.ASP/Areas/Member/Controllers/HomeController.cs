﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Techno.ASP.Models;
using Techno.ASP.Tools;

namespace Techno.ASP.Areas.Member.Controllers
{
    [Area("Member")] //OBLIGATOIRE
    [Authorize(Policy = "RoleStudentPolicy")]
    public class HomeController : Controller
    {
        public IActionResult Index()
        { 
                UserModel um = SessionUtils.ConnectedUser;
                TempData["success"] = $"Bienvenue {um.FirstName}";
                return View();
             
        }

        public IActionResult Logout()
        {
            //Processus de Logout
            HttpContext.Session.Clear();
            //Redirection vers la page principale du site
            return RedirectToAction("Index", "Home", new { area = "" });

        }

    }
}
