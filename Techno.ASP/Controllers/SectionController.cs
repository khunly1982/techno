﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Techno.ASP.Models;
using Techno.ASP.Models.Forms;
using Techno.ASP.Services;

namespace Techno.ASP.Controllers
{
    public class SectionController : Controller
    {
        private readonly IService<SectionModel,SectionForm> _service;

        public SectionController(IService<SectionModel, SectionForm> service)
        {
            _service = service;
        }

        public IActionResult Index()
        {
            IEnumerable<SectionModel> model = _service.GetAll();
            return View(model);
        }

        //affichage
        public IActionResult Create()
        {
            return View();
        }

        //retour traitement
        [HttpPost]
        public IActionResult Create(SectionForm form)
        {
            // correct?
            // si oui
            if (ModelState.IsValid)
            {
                // service pour inserer
                _service.Insert(form);
                // Notification
                TempData["success"] = "Insertion OK";
                // redirection
                return RedirectToAction("Index");
            }
            else
            {
                // si non
                // réafficher la vue avec le form et les erreurs
                return View(form);
            }
        }

        // affichage
        public IActionResult Update([FromRoute] int id)
        {
            SectionForm form = _service.GetById(id);
            if (form == null) return NotFound();
            return View(form);
        }
        //traitement
        [HttpPost]
        public IActionResult Update(SectionForm form)
        {
            if (ModelState.IsValid)
            {
                _service.Update(form);
                TempData["success"] = "Modification OK";
                return RedirectToAction("Index");
            }
            else
            {
                return View(form);
            }
        }

        public IActionResult Delete([FromRoute] int id)
        {
            if (_service.Delete(id))
            {
                TempData["success"] = "Suppression OK";
            }
            else
            {
                TempData["error"] = "Ca n'a pas fonctionné";
            }
            return RedirectToAction("Index");
        }
    }
}
