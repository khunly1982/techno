﻿using Microsoft.AspNetCore.Mvc;
using Techno.ASP.Models;
using Techno.ASP.Models.Forms;
using Techno.ASP.Services;
using Techno.ASP.Tools;
namespace Techno.ASP.Controllers
{
    public class SecurityController : Controller
    {
        IService<UserModel, RegisterUserForm> _service;

        public SecurityController(IService<UserModel, RegisterUserForm> service)
        {
            _service = service;
        }
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
            public IActionResult Login(LoginModel form, string ReturnUrl)
        {



            if(ModelState.IsValid)
            {
                //Processus de vérif dans la db
                UserModel um = (_service as UserService).GetByEmailPassword(form.Email, form.Password);
                if (um != null)
                {
                    //HttpContext.Session.Set<bool>("IsLogged", true);
                    //HttpContext.Session.Set<UserModel>("ConnectedUser", um);
                    SessionUtils.ConnectedUser = um;
                    SessionUtils.IsLogged = true;

                    if(ReturnUrl != null)
                    {
                        if (Url.IsLocalUrl(ReturnUrl))
                            return Redirect(ReturnUrl);
                    }
                    return RedirectToAction("Index", "Home", new { are = "Membre" });
                }
                else
                {
                    TempData["error"] = "Invalid Email or Password";
                    return View();
                }
            }
            else
            {
                return View();
            }
            
        }


    }
}
