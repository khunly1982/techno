﻿using MailKit.Net.Smtp;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Techno.ASP.Models;
using Techno.ASP.Models.Forms;
using Techno.ASP.Services;
using Techno.ASP.Tools;
namespace Techno.ASP.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        private readonly Mailer _mailer;

        // injection de dépendances par constructeur
        public HomeController(ILogger<HomeController> logger, Mailer mailer)
        {
            _logger = logger;
            _mailer = mailer;
        }

        public IActionResult Index()
        {

            TempData["isLogged"] = HttpContext.Session.Get<bool>("IsLogged");
           
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult AboutMe()
        {
            AboutMeModel model = new AboutMeModel
            {
                Nom = "Ly",
                Prenom = "Khun",
                DateDeNaissance = new DateTime(1982,5,6)
            };

            return View(model);
        }

        // afficher le formulaire
        public IActionResult Contact()
        {
            return View();
        }

        // traiter le formulaire
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Contact([FromForm]ContactForm form)
        {
            // verifier si le formulaire est valide
            // si oui on envoie un email + redirection
            if(ModelState.IsValid)
            {
                // envoyer un email
                string sub = "Vous avez recu un message de la part de " + form.Email;
                string content = $"<p>{form.Message}</p>";
                bool result = _mailer.Send("lykhun@gmail.com", sub, content);
                if(result)
                {
                    // notification success
                    TempData["success"] = "Votre message a bien été envoyé";
                }
                else
                {
                    // notification erreur
                    TempData["error"] = "Une erreur est survenue";
                }
                ViewBag.Info = "copperfield";
                // rediriger
                return RedirectToAction("Index");
            }
            // si non on reste sur la page et on affiche les erreurs 
            else
            {
                
                return View(form);
            }
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
