﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Techno.ASP.Models;
using Techno.ASP.Models.Forms;
using Techno.ASP.Services;
using Techno.ASP.Tools;
namespace Techno.ASP.Controllers
{
    public class ProfileController : Controller
    {
        IService<SectionModel, SectionForm> _sectionService;
        IService<ProfileModel, ProfileForm> _service;
        IService<SkillModel, SkillForm> _skillService;
        public ProfileController(
            IService<ProfileModel, ProfileForm> service,
            IService<SectionModel,SectionForm> SectionService,
            IService<SkillModel, SkillForm> SkillService)
        {
            _service = service;
            _sectionService = SectionService;
            _skillService = SkillService;
        }

        public IActionResult Index()
        {
            return View();
        }

       
        public IActionResult Create(int id)
        {
            //Ici l'id sera l'id de l'utilisateur associé au futur profil
            ProfileForm form = new ProfileForm(_sectionService, _skillService) 
            { 
                UserId =id, 
                Description=HttpContext.Session.Get<string>("Test")
            };
            return View(form);
        }

        [HttpPost]
        public IActionResult Create(ProfileForm form)
        {
            //1- Utiliser le this.Request.Form qui représente le formulaire envoyé
            //   A l'intérieur, je vais avoir une clé = ChoixSkill

            string[] MesChoix = this.Request.Form["ChoixSkill"].ToArray();

            //2- remplir mon model Profileform avec les choix fait par l'utilisateur
            form.SkillIds = new List<int>();
            foreach (string item in MesChoix)
            {
                form.SkillIds.Add(int.Parse(item));
            }

            //Pour éviter le pattern ServiceLocator (Petite ruse)On injecte directement le service
            // via la propriété de notre objet
            form.SectionService = _sectionService;

            if (ModelState.IsValid)
            {
                string[] TypeAutorise = { "image/jpeg", "image/jpg", "image/png", "image/gif" };
                byte[] imageEnByte = null;
                //1- checker note fichier
                //1.1 Est-ce qu'on a un fichier?
                if (form.ImageFile != null)
                {
                    //1.2 La taille est -elle correcte?
                    if (form.ImageFile.Length > 10000000) //max 10 Mo
                    {
                        TempData["error"] = "Image trop lourde";
                        return View(form);
                    }
                       //1.3 Est-ce une image ?
                    if(!TypeAutorise.Contains(form.ImageFile.ContentType))
                    {
                       TempData["error"] = "le format de l'image n'est pas autorisé";
                        HttpContext.Session.Set<ProfileForm>("Profile", form);
                        return View(form);
                    }
                }
                _service.Insert(form); 
                //return View(form);
                //Redirigder vers sa zone Membre
                return RedirectToAction("Index", "Home", new { area = "Member" });
            }
            else
            {
                TempData["error"] = "Erreur d'enregistrement";
                return View(form);
            }
        }


        public IActionResult Update()
        {
            return View();
        }

        public IActionResult Delete()
        {
            return RedirectToAction("Index");
        }

        public IActionResult Details()
        {
            return View();
        }
    }
}
