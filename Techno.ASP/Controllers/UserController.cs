﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Techno.ASP.Models;
using Techno.ASP.Models.Forms;
using Techno.ASP.Services;
using Techno.ASP.Tools;
namespace Techno.ASP.Controllers
{
    public class UserController : Controller
    {
        IService<UserModel, RegisterUserForm> _service;
        public UserController(IService<UserModel,RegisterUserForm> service)
        {
            _service = service;
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Create(RegisterUserForm form)
        {
            if(ModelState.IsValid)
            {
                _service.Insert(form);
                TempData["success"] = "Formulaire ok";
                HttpContext.Session.Set<bool>("IsLogged", true);
                //Je redirige vers la création de profile
                return RedirectToAction("Create", "Profile",new { id = form.Id }); //L'id
            }
            else
            {
                TempData["error"] = "Formulaire invalide";
                return View(form);
            }
        }
    }
}
