﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Techno.ASP.Models;

namespace Techno.ASP.ViewComponents
{
    public class MeteoViewComponent : ViewComponent
    {
        private readonly string url = "https://api.openweathermap.org/data/2.5/weather";

        private readonly IConfiguration _config;
        private readonly HttpClient _client;

        public MeteoViewComponent(IConfiguration config, HttpClient client)
        {
            _config = config;
            _client = client;
        }

        public IViewComponentResult Invoke()
        {
            HttpResponseMessage message = _client.GetAsync(
                url + "?q=charleroi&APPID=" + _config.GetSection("ApiKeys").GetValue<string>("OPWM") + "&units=metric"
            ).Result;
            string json = message.Content.ReadAsStringAsync().Result;
            MeteoModel model = JsonConvert.DeserializeObject<MeteoModel>(json);
            return View(model);
        }
    }
}
