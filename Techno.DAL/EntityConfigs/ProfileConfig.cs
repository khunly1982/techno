﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Techno.DAL.Entities;

namespace Techno.DAL.EntityConfigs
{
    class ProfileConfig : IEntityTypeConfiguration<Profile>
    {
        public void Configure(EntityTypeBuilder<Profile> builder)
        {
            builder.ToTable(nameof(Profile));

            builder.HasKey(p => p.Id);

            builder.HasOne(p => p.User)
                .WithOne(u => u.Profile)
                .HasForeignKey<Profile>(p => p.UserId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasOne(p => p.Section)
                .WithMany(s => s.Profiles)
                .OnDelete(DeleteBehavior.SetNull);

            // Retiré car on a ajouté l'entité pour la table intermédiaire
            //builder.HasMany(p => p.Skills)
            //    .WithMany(s => s.Profiles);

            builder.HasMany(p => p.ProfilSkills)
                .WithOne(ps => ps.Profile);
                

            builder.Property(p => p.ImageMimeType)
                .HasMaxLength(50);

            builder.Property(p => p.Linkedin)
                .HasMaxLength(255);

            builder.Property(p => p.Hobbies)
                .HasMaxLength(255);

            builder.Property(p => p.Published)
                .IsRequired()
                .HasDefaultValue(false);
        }
    }
}
