﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Techno.DAL.Entities;

namespace Techno.DAL.EntityConfigs
{
    public class ProfileSkillConfig : IEntityTypeConfiguration<ProfileSkill>
    {
        public void Configure(EntityTypeBuilder<ProfileSkill> builder)
        {
            builder.ToTable(nameof(ProfileSkill));

            builder.HasKey(p => new { p.ProfileId, p.SkillId }); //Double primary key plutôt que d'ajouter un Id

           

        }
    }
}
