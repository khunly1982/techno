﻿using System;
using Techno.DAL.Enums;

namespace Techno.DAL.Entities
{
    public class User
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public byte[] Password { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public DateTime? BirthDate { get; set; }
        public Role Role { get; set; }

        public virtual Profile Profile { get; set; }
    }
}
