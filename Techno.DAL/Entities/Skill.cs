﻿using System.Collections.Generic;

namespace Techno.DAL.Entities
{
    public class Skill
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual IEnumerable<ProfileSkill> ProfileSkill { get; set; }
    }
}
