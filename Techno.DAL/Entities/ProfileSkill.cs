﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Techno.DAL.Entities
{
    public class ProfileSkill
    {
        public int ProfileId { get; set; }
        public int SkillId { get; set; }

        public virtual Skill Skill { get; set; }
        public virtual Profile Profile { get; set; }
    }
}
