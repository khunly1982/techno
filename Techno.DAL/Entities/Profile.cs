﻿using System.Collections.Generic;

namespace Techno.DAL.Entities
{
    public class Profile
    {
        public int Id { get; set; }
        public byte[] ImageFile { get; set; }
        public string ImageMimeType { get; set; }
        public string Description { get; set; }
        public string Linkedin { get; set; }
        public string Hobbies { get; set; }
        public string Studies { get; set; }
        public string ProfessionalExp { get; set; }
        public bool Published { get; set; }

        public int UserId { get; set; }
        public int? SectionId { get; set; }

        public virtual User User { get; set; }
        public virtual Section Section { get; set; }
        public virtual IEnumerable<ProfileSkill> ProfilSkills { get; set; }
    }
}
