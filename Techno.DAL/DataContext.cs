﻿using Microsoft.EntityFrameworkCore;
using Techno.DAL.Entities;
using Techno.DAL.EntityConfigs;

namespace Techno.DAL
{
    public class DataContext: DbContext
    {
        private readonly string _defaultConnectionString = @"data source=(localdb)\MSSQLLocalDB;initial catalog=demo_asp;integrated security=true";
        
        public DbSet<User> Users { get; set; }
        public DbSet<Profile> Profiles { get; set; }
        public DbSet<Skill> Skills { get; set; }
        public DbSet<Section> Sections { get; set; }

        public DbSet<ProfileSkill> ProfileSkills { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_defaultConnectionString);
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new UserConfig());
            builder.ApplyConfiguration(new ProfileConfig());
            builder.ApplyConfiguration(new SectionConfig());
            builder.ApplyConfiguration(new SkillConfig());
            builder.ApplyConfiguration(new ProfileSkillConfig());
        }
    }
}
