﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Techno.DAL;
using Techno.DAL.Entities;
using Techno.DAL.Enums;

namespace Techno.Test
{
    [TestClass]
    public class DataContextTest
    {
        private DataContext _context;

        [TestInitialize]
        public void Init()
        {
            _context = new DataContext();
        }

        [TestMethod]
        public void TestInsertUserWithBadEmail__throwsException()
        {
            User u = new User {
                LastName = "Ly",
                FirstName = "Khun",
                Password = new byte[0],
                Role = Role.Admin,
                Email = "test"
            };

            Assert.ThrowsException<DbUpdateException>(() =>
            {
                _context.Users.Add(u);
                _context.SaveChanges();
            });
        }

        [TestMethod]
        public void TestInsertUser__returns_1()
        {
            User u = new User
            {
                LastName = "Ly",
                FirstName = "Khun",
                Password = new byte[0],
                Role = Role.Admin,
                Email = "test@test"
            };
            _context.Users.Add(u);
            int nb = _context.SaveChanges();
            Assert.AreEqual(nb, 1);

        }
    }
}
